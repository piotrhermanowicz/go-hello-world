package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func (responseWriter http.ResponseWriter, request *http.Request)  {
		n, error := fmt.Fprintf(responseWriter, "Hello,  world!")

		if error != nil {
			fmt.Println(error)
		}

		fmt.Println(fmt.Sprintf("Number of bytes written %d", n))

	})

	_ = http.ListenAndServe(":8080", nil)
}